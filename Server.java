import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Random;

/**
  * A simple concurrent web server that is designed by the standard set in the
  * RFC2616. This web server also gathers data on picture comparisions if a
  * html document has tags to specify picture comparison. Data gathered is
  * into the log file.
  * @author B[]Array
  * @version 1.4
  **/
public class Server extends Thread{
  private static final int DEFAULT_PORT = 8080;
  private static final String DEFAULT_LOG_FILE = "logOutput.log";
  private static final int SEARCH_LENGTH = 20;
  private static final String BR = "\r\n";
  private static final String DIRECTORY = "www";
  private static final String RETURN_200 = "OK", MESSAGE_200 = "";
  private static final String RETURN_404 = "Not Found", MESSAGE_404 = "<h1>Requested file not found.</h1>";
  private static final String RETURN_405 = "Method Not Allowed", MESSAGE_405 = "<h1>Method not allowed</h1>";
  private static final String RETURN_501 = "Not Implemented", MESSAGE_501 = "<h1>Not implemented</h1>";
  private static final String CONNECTION_CLOSE = "Connection: close";
  private static final String CONNECTION_ALLOW = CONNECTION_CLOSE + BR + "Allow: GET, HEAD";
  private static boolean debug = false;
  private static FileOutputStream logFile;
  private static PrintStream logOut;
  private File file;
  private SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
  private Socket conn;

  /**
    * A simple enumerator to allow a return code to relate directly to
    * information that is required to be output to the client.
    **/
  private enum Code{
    GET(200, RETURN_200, CONNECTION_CLOSE, MESSAGE_200),
    HEAD(200, RETURN_200, CONNECTION_CLOSE, MESSAGE_200),
    NOT_FOUND(404, RETURN_404, CONNECTION_CLOSE, MESSAGE_404),
    PUT(405, RETURN_405, CONNECTION_ALLOW, MESSAGE_405),
    DELETE(405, RETURN_405, CONNECTION_ALLOW, MESSAGE_405),
    TRACE(405, RETURN_405, CONNECTION_ALLOW, MESSAGE_405),
    UNKNOWN(501, RETURN_501, CONNECTION_CLOSE, MESSAGE_501);
    private final int code;
    private final String returnString;
    private final String returnResponse;
    private final String returnMessage;

    Code(int code, String returnString, String returnResponse, String returnMessage){
      this.code = code;
      this.returnString = returnString;
      this.returnResponse = returnResponse;
      this.returnMessage = returnMessage;
    }

    int getCode(){ return code; }

    String getReturn(){ return returnString; }

    String getResponse(){ return returnResponse; }

    String getMessage(){ return returnMessage; }
  }

  /**
    * Execution entry point for the web server. This contains the main loop and
    * distributes clients out to seperate threads in order to listen for more
    * clients.
    * @param args An array containing parameter command line arguements given
    * by the user. Currently, first specifies port, second an output log file
    * and third turns on visual debugging with the word "verbose".
    **/
  public static void main(String args[]) throws Exception{
    //Run from a given port if one is specified
    int port = DEFAULT_PORT;
    try{ if(args.length>0)port = Integer.parseInt(args[0]); }
    catch(NumberFormatException e){ output("Input port number was not valid."); }
    //Create the server socket
    ServerSocket serverSock = new ServerSocket(port);

    //Get filename to log to if it exists
    String logFileName = DEFAULT_LOG_FILE;
    if(args.length>1)logFileName = args[1];
    //Create logfile Ouput
    logFile = new FileOutputStream(new File(logFileName), true);
    logOut = new PrintStream(logFile, true);

    //Check to see if debug mode set
    if(args.length>2)if(args[2].toLowerCase().equals("verbose"))debug = true;

    while(true){ //Loop server infinitaly
      Socket conn = serverSock.accept();
      new Server(conn).start();
    }
  }

  /**
    * Constructor simply accepts pointer to socket connection from the main
    * Thread and makes it globally available to the rest of the program.
    * @param conn The connection to the client to be passed the created Thread.
    **/
  public Server(Socket conn){ this.conn = conn; }

  @Override
  public void run(){
    output("Thread '" + this.getId() + "' started."); //Debug

    //Method used variables
    Code command;
    FileInputStream ins;
    OutputStream outs;

    try{
      Scanner scanin = new Scanner(conn.getInputStream());
      String line = null;
      String[] storedLines = new String[SEARCH_LENGTH];
      int nlines = 0;

      while(true){
        if(!scanin.hasNext())break;
        line = scanin.next();
        if(line==null||nlines>=SEARCH_LENGTH)break;
        storedLines[nlines++] = line;
      }

      if(storedLines[0]!=null){ //Check to see if connection timed out
        command = Code.valueOf(storedLines[0].toUpperCase()); //Don't assume client sends request in correct case
        if(command==null)command = Code.UNKNOWN; //If code could not be found then it's not implemented
        String location = DIRECTORY + storedLines[1];

        //Check file
        file = new File(location);
        if(file.isDirectory()){
          if(location.charAt(location.length() - 1)!='/')location += '/';
          File test = new File(location + "index.htm");
          if(test.isFile())file = test;
          test = new File(location + "index.html");
          if(test.isFile())file = test;
        }

        //If File doesn't exist - it's our special code!
        if(!file.isFile()){
          if(file.getName().substring(0, 6).equals("result")){
            log(file.getName().substring(7));
            file = new File(DIRECTORY + "/index.html");
          }
        }

        //Finally deal with input and output
        outs = conn.getOutputStream();
        ins = null;

        if(command.getCode()!=200)ins = null;
        else{
          if(!file.isFile())command = Code.NOT_FOUND;
          else ins = new FileInputStream(file);
        }

        if(getContentType(ins).equals("text/html")){
          String body = "";

          if(ins!=null&&command!=Code.HEAD){ //Make sure file exists and we are requesting a file
            byte buffer[] = new byte[1024];
            while(true){
              int rc = ins.read(buffer, 0, 1024);
              if(rc<=0)break;
              if(getContentType(ins).equals("text/html")){
                for(int x=0; x<rc-4; x++){
                  if(buffer[x]=='#'&&buffer[x+1]=='I'&&buffer[x+2]=='M'&&buffer[x+3]=='G'&&buffer[x+4]=='#'){
                    //outs.write(buffer, 0, x);
                    body += new String(buffer).substring(0, x);
                    byte[] images = insertLinks();
                    //outs.write(images, 0, images.length);
                    body += new String(images, 0, images.length);
                    //outs.write(buffer, x + 5, rc);
                    body += new String(buffer).substring(x + 5, rc);
                    rc = 0;
                  }
                }
              }else{
                //outs.write(buffer, 0, rc);
                body += new String(buffer);
              }
            }
          }

          String reply =  "HTTP/1.0 " + command.getCode() + " " + command.getReturn() + BR;
                 reply += command.getResponse() + BR;
                 if(ins!=null)reply += "Content-Length: " + (body.length()) + BR; //TODO: Fix bug on file length (incorrect once image sent)
                 reply += "Content-Type: " + getContentType(ins) + BR;
                 reply += "Date: " + dateFormat.format(Calendar.getInstance().getTime()) + BR;
                 reply += BR;
                 reply += command.getMessage();
          outs.write(reply.getBytes()); //Start sending the first part.

          //for(int x=0; x<body.length(); x+=1024){
          //  if(x+1024<body.length())outs.write(body.substring(x, x + 1024).getBytes(), 0, 1024); //Send chaunk of data
          //  else outs.write(body.substring(x).getBytes(), 0, body.length());
          //}
          outs.write(body.getBytes());

          output(reply); //Debug
        }else{
          //Build reply from data gathered
          String reply =  "HTTP/1.0 " + command.getCode() + " " + command.getReturn() + BR;
                 reply += command.getResponse() + BR;
                 if(ins!=null)reply += "Content-Length: " + file.length() + BR;
                 reply += "Content-Type: " + getContentType(ins) + BR;
                 reply += "Date: " + dateFormat.format(Calendar.getInstance().getTime()) + BR;
                 reply += BR;
                 reply += command.getMessage();
          outs.write(reply.getBytes()); //Start sending the first part.

          output(reply); //Debug

          if(ins!=null&&command!=Code.HEAD){ //Make sure file exists and we are requesting a file
            byte buffer[] = new byte[256];
            while(true){
              int rc = ins.read(buffer, 0, 256);
              if(rc<=0)break;
              outs.write(buffer, 0, rc);
            }
          }
        }
      }
      conn.close(); //Close the socket connection
    }catch(Exception e){
      output("Something broke " + e.getMessage());
      e.printStackTrace();
    }
    output("Thread '" + this.getId() + "' ended."); //Debug
  }

  private byte[] insertLinks(){
    int rand1 = new Random().nextInt(4);
    int rand2 = new Random().nextInt(4);
    while(rand1==rand2)rand2 = new Random().nextInt(4);
    String output = "<a href=\"result?" + rand1 + "%" + rand2 + "%" + rand1 + "\"><img src=\"" + rand1 + ".jpg\"></a><br>";
    output += "<a href=\"result?" + rand1 + "%" + rand2 + "%" + rand2 + "\"><img src=\"" + rand2 + ".jpg\"></a><br>";
    return output.getBytes();
  }

  private String getContentType(InputStream ins){
    if(ins==null) return "text/html";
    String extention = file.getName();
    extention = extention.substring(extention.lastIndexOf('.') + 1, extention.length());
    output(extention);
    if(extention.equals("jpg")||extention.equals("jpeg"))return "image/jpeg";
    if(extention.equals("htm")||extention.equals("html"))return "text/html";
    return "text/plain";
  }

  private static void output(String msg){ if(debug)System.out.println("Debug::" + msg); }

  private static void log(String msg){ logOut.println(msg); }
}
